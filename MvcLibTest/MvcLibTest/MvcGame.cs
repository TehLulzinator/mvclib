using GameManagement;
using Herman.MvcLib.Xna;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MvcLibTest
{
    public class MvcGame : Game
    {
        GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;
        private GameLoop _gameLoop;
        private InputHelper _inputHelper;

        public MvcGame()
        {
            _inputHelper = new InputHelper();
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            _gameLoop = new GameLoop();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(_graphics.GraphicsDevice);
            var font = Content.Load<SpriteFont>("default");
            DrawingHelper.Initialize(GraphicsDevice, font);

            _gameLoop.AddView(new RectangleView(100, 100, 50, 50)
            {
                Color = Color.DarkRed,
                DrawOrder = 5,
                Type = RectangleView.RectangleType.Bordered
            
            });
            _gameLoop.AddView(new RectangleView(90, 90, 50, 50)
            {
                Color = Color.ForestGreen,
                DrawOrder = -2,
                Type = RectangleView.RectangleType.Filled
            });
            _gameLoop.AddView(new RectangleView(120, 80, 100, 85)
            {
                BorderColor = Color.LimeGreen,
                DrawOrder = -5,
                Type = RectangleView.RectangleType.BorderOnly
            });
            _gameLoop.AddView(new RectangleView(20, 105, 150, 30)
            {
                Color = Color.Beige,
                DrawOrder = 2,
                Type = RectangleView.RectangleType.Filled
            });
        }

        protected override void Update(GameTime gameTime)
        {
            _inputHelper.Update();

            if (_inputHelper.KeyPressed(Keys.Escape))
                Exit();

            _gameLoop.UpdateModels(gameTime);
            _gameLoop.HandleControllers(_inputHelper);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DimGray);

            _spriteBatch.Begin();
            _gameLoop.DrawViews(_spriteBatch);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
