using System;
using GameManagement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Displays a rectangle. Can be filled, bordered or wireframed
    /// </summary>
    public class RectangleView : ViewObject
    {
        /// <summary>
        /// The different drawing types of a rectangle
        /// </summary>
        public enum RectangleType
        {
            /// <summary>
            /// A rectangle filled with a color. Uses the Color property.
            /// </summary>
            Filled,
            /// <summary>
            /// A filled rectangle with a border. Uses the Color and BorderColor properties.
            /// </summary>
            Bordered,
            /// <summary>
            /// Only the border of a rectangle. Uses the BorderColor property.
            /// </summary>
            BorderOnly
        };

        /// <summary>
        /// The width of the rectangle.
        /// </summary>
        public int Width;

        /// <summary>
        /// The height of the rectangle.
        /// </summary>
        public int Height;

        /// <summary>
        /// The type of rectangle that this view displays.
        /// </summary>
        public RectangleType Type;

        /// <summary>
        /// The color of the border of the rectangle, if the border is displayed.
        /// </summary>
        public Color BorderColor;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <param name="width">Width of the rectangle.</param>
        /// <param name="height">Height of the rectangle.</param>
        public RectangleView(int x, int y, int width, int height)
        {
            LocalPosition = new Vector2(x, y);
            Width = width;
            Height = height;
            Color = Color.White;
            BorderColor = Color.Black;
            Type = RectangleType.Bordered;
        }

        protected override void DoDraw(SpriteBatch spriteBatch)
        {
            var r = new Rectangle((int)Position.X, (int)Position.Y, Width, Height);
            switch (Type)
            {
                case RectangleType.Filled:
                    DrawingHelper.DrawRectangle(r, spriteBatch, Color, 1, true);
                    break;
                case RectangleType.Bordered:
                    DrawingHelper.DrawRectangle(r, spriteBatch, Color, 1, true);
                    DrawingHelper.DrawRectangle(r, spriteBatch, BorderColor);
                    break;
                case RectangleType.BorderOnly:
                    DrawingHelper.DrawRectangle(r, spriteBatch, BorderColor);
                    break;
            }
        }

        public override bool Contains(Point point)
        {
            throw new NotImplementedException();
        }
    }
}