﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Base class for a drawable object.
    /// </summary>
    public abstract class ViewObject
    {
        private int _drawOrder;
        public ViewList Parent;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        protected ViewObject()
        {
            Visible = true;
            _drawOrder = 0;
            Color = Color.White;
            LocalPosition = Vector2.Zero;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="drawOrder">The order in which object gets drawn. If you know this value at constructor time always set it through the constructor, not afterwards!</param>
        /// <param name="position">The starting position of object</param>
        protected ViewObject(int drawOrder, Vector2 position)
        {
            _drawOrder = drawOrder;
            Color = Color.White;
            LocalPosition = position;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="drawOrder">The order in which object gets drawn. If you know this value at constructor time always set it through the constructor, not afterwards!</param>
        /// <param name="position">The starting position of object</param>
        /// <param name="color">The color of object</param>
        protected ViewObject(int drawOrder, Vector2 position, Color color)
        {
            _drawOrder = drawOrder;
            Color = color;
            LocalPosition = position;
        }

        /// <summary>
        /// The color to draw the object in.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Position relative to parent.
        /// </summary>
        public Vector2 LocalPosition { get; set; }

        /// <summary>
        /// Absolute position in the gameworld taking parent position into account.
        /// </summary>
        public Vector2 Position
        {
            get { return Parent == null ? LocalPosition : LocalPosition + Parent.Position; }
        }

        /// <summary>
        /// Whether the view should be drawn.
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        /// The scale at which this object should get drawn.
        /// </summary>
        public float Scale { get; set; }

        /// <summary>
        /// The order in which the object should be drawn, relative to its sibblings. Lower is farther behind, higher is more in front.
        /// </summary>
        public int DrawOrder
        {
            get { return _drawOrder; }
            set
            {
                _drawOrder = value;
                OnDrawOrderChanged(new EventArgs());
            }
        }

        /// <summary>
        /// Called when the DrawOrder property changes
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnDrawOrderChanged(EventArgs e)
        {
            if (DrawOrderChanged != null)
                DrawOrderChanged(this, e);
        }

        /// <summary>
        /// Raised when the DrawOrder property changes
        /// </summary>
        public event EventHandler DrawOrderChanged;

        /// <summary>
        /// Draws the object
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!Visible)
                return;

            DoDraw(spriteBatch);
        }

        /// <summary>
        /// Override this to add custom drawing logic. Does not get called if Visible is set to false.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch to draw with</param>
        protected abstract void DoDraw(SpriteBatch spriteBatch);

        /// <summary>
        /// Determines whether a point overlaps this object
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public abstract bool Contains(Point point);
    }
}