using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Holds a list of models, enforcing the right update order.
    /// </summary>
    public class ModelList : ModelObject
    {
        private List<ModelObject> _models;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ModelList()
        {
            _models = new List<ModelObject>();
        }

        /// <summary>
        /// Add a model to the list, including it in the update loop.
        /// </summary>
        /// <param name="model">The model to add.</param>
        public void Add(ModelObject model)
        {
            model.UpdateOrderChanged += ModelUpdateOrderChanged;
            for (int i = 0; i < _models.Count; i++)
            {
                if (model.UpdateOrder < _models[i].UpdateOrder)
                {
                    _models.Insert(i, model);
                    return;
                }
            }
            _models.Add(model);
        }

        /// <summary>
        /// Remove a model from the list, excluding it from the update loop.
        /// </summary>
        /// <param name="model">The model to add.</param>
        public void Remove(ModelObject model)
        {
            _models.Remove(model);
        }

        protected override void DoUpdate(GameTime gameTime)
        {
            foreach (ModelObject m in _models)
                m.Update(gameTime);
        }

        private void ModelUpdateOrderChanged(object sender, EventArgs e)
        {
            var m = sender as ModelObject;
            if (m == null) throw new ArgumentNullException("sender", "Could not cast sender to ModelObject.");
            Remove(m);
            Add(m);
        }
    }
}