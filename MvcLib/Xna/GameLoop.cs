﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Contains models, views and controllers for your main game loop.
    /// </summary>
    public class GameLoop
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public GameLoop()
        {
            Models = new ModelList();
            Views = new ViewList();
            Controllers = new ControllerList();
        }

        /// <summary>
        /// List of all controllers in the game loop.
        /// </summary>
        private ControllerList Controllers { get; set; }

        /// <summary>
        /// List of all models in the game loop.
        /// </summary>
        private ModelList Models { get; set; }

        /// <summary>
        /// List of all views in the game loop
        /// </summary>
        private ViewList Views { get; set; }

        /// <summary>
        /// Initializes new instances of all the lists, removing all previous models, views and controllers.
        /// </summary>
        public void Reset()
        {
            Models = new ModelList();
            Views = new ViewList();
            Controllers = new ControllerList();
        }

        /// <summary>
        /// Updates all models in the loop.
        /// </summary>
        /// <param name="gameTime"></param>
        public void UpdateModels(GameTime gameTime)
        {
            Models.Update(gameTime);
        }

        /// <summary>
        /// Drass all the views in the loop.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void DrawViews(SpriteBatch spriteBatch)
        {
            Views.Draw(spriteBatch);
        }

        /// <summary>
        /// Handles input on all controllers in the loop.
        /// </summary>
        /// <param name="inputHelper"></param>
        public void HandleControllers(InputHelper inputHelper)
        {
            Controllers.HandleInput(inputHelper);
        }

        /// <summary>
        /// Adds a model to the game loop.
        /// </summary>
        /// <param name="model"></param>
        public void AddModel(ModelObject model)
        {
            Models.Add(model);
        }

        /// <summary>
        /// Removes a model from the game loop
        /// </summary>
        /// <param name="model"></param>
        public void RemoveModel(ModelObject model)
        {
            Models.Remove(model);
        }

        /// <summary>
        /// Adds a view to the game loop.
        /// </summary>
        /// <param name="view"></param>
        public void AddView(ViewObject view)
        {
            Views.Add(view);
        }

        /// <summary>
        /// Removes a view from the game loop.
        /// </summary>
        /// <param name="view"></param>
        public void RemoveView(ViewObject view)
        {
            Views.Remove(view);
        }

        /// <summary>
        /// Adds a controller to the game loop.
        /// </summary>
        /// <param name="controller"></param>
        public void AddController(ControllerObject controller)
        {
            controller.GameLoop = this;
            Controllers.Add(controller);
        }

        /// <summary>
        /// Removes a controller from the game loop.
        /// </summary>
        /// <param name="controller"></param>
        public void RemoveController(ControllerObject controller)
        {
            Controllers.Remove(controller);
        }
    }
}