﻿using System;
using Microsoft.Xna.Framework;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Class for holding and updating data about your game model.
    /// </summary>
    public abstract class ModelObject
    {
        private int _updateOrder;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        protected ModelObject()
        {
            Active = true;
            _updateOrder = 0;
        }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="updateOrder">The order in which to update this model. Lower is before others. Negative values are allowed.</param>
        protected ModelObject(int updateOrder)
        {
            _updateOrder = updateOrder;
        }

        /// <summary>
        /// Whether this model should get updated.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Defines the order in which this model is updated. Lower is before other models. Negative values are allowed.
        /// Beware that update order is always relative to sibblings and cannot surpass the parent update order.
        /// </summary>
        public int UpdateOrder
        {
            get { return _updateOrder; }
            set
            {
                _updateOrder = value;
                UpdateOrderChanged(this, new EventArgs());
            }
        }

        /// <summary>
        /// Updates the model.
        /// </summary>
        /// <param name="gameTime">Current gametime.</param>
        public void Update(GameTime gameTime)
        {
            if (!Active)
                return;

            DoUpdate(gameTime);
        }

        /// <summary>
        /// Called when the UpdateOrder property has changed.
        /// </summary>
        /// <param name="e"></param>
        protected void OnUpdateOrderChanged(EventArgs e)
        {
            if (UpdateOrderChanged != null)
                UpdateOrderChanged(this, e);
        }

        /// <summary>
        /// Raised when the UpdateOrder property has changed.
        /// </summary>
        public event EventHandler UpdateOrderChanged;

        /// <summary>
        /// Raised when any public property of the model has changed.
        /// </summary>
        public event EventHandler Changed;

        /// <summary>
        /// Called when the model has changed.
        /// </summary>
        /// <param name="e"></param>
        protected void OnChanged(EventArgs e)
        {
            if (Changed != null)
                Changed(this, e);
        }

        /// <summary>
        /// Override this to add custom update logic to your model.
        /// </summary>
        /// <param name="gameTime">Current gametime.</param>
        protected abstract void DoUpdate(GameTime gameTime);
    }
}