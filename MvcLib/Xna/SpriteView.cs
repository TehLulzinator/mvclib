using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Drawable object with a texture
    /// </summary>
    public class SpriteView : ViewObject
    {
        /// <summary>
        /// Texture representing this object
        /// </summary>
        public Texture2D Texture;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="texture">Texture drawn to represent this object</param>
        public SpriteView(Texture2D texture)
        {
            Texture = texture;
        }

        /// <summary>
        /// Draw this object
        /// </summary>
        /// <param name="spriteBatch">Spritebatch to draw with</param>
        protected override void DoDraw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, LocalPosition, null, Color, 0f, Vector2.Zero, new Vector2(Scale),
                SpriteEffects.None, 0f);
        }

        public override bool Contains(Point point)
        {
            // TODO: Implement pixel perfect sprite collision here
            return Texture.Bounds.Contains(point);
        }
    }
}