using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// A list of <c>ViewObject</c>s with methods for adding and removing them.
    /// </summary>
    public class ViewList : ViewObject
    {
        private List<ViewObject> _views;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public ViewList()
        {
            _views = new List<ViewObject>();
        }

        /// <summary>
        /// Add a view to the list, adding them to the object's draw loop.
        /// </summary>
        /// <param name="view">The view to add.</param>
        public void Add(ViewObject view)
        {
            view.Parent = this;
            view.DrawOrderChanged += ViewDrawOrderChanged;
            for (int i = 0; i < _views.Count; i++)
            {
                if (view.DrawOrder < _views[i].DrawOrder)
                {
                    _views.Insert(i, view);
                    return;
                }
            }
            _views.Add(view);
        }

        /// <summary>
        /// Remove a view from the list, excluding them from the draw loop.
        /// </summary>
        /// <param name="view">The view to remove.</param>
        public void Remove(ViewObject view)
        {
            _views.Remove(view);
        }

        private void ViewDrawOrderChanged(object sender, EventArgs e)
        {
            var v = sender as ViewObject;
            if (v == null) throw new ArgumentNullException("sender", "Could not cast sender to ViewObject");
            Remove(v);
            Add(v);
        }

        /// <summary>
        /// Draws this view and all its children.
        /// </summary>
        /// <param name="spriteBatch">The spritebatch to draw with.</param>
        protected override sealed void DoDraw(SpriteBatch spriteBatch)
        {
            if (!Visible)
                return;

            foreach (ViewObject v in _views)
                v.Draw(spriteBatch);
        }

        /// <summary>
        /// Checks all child objects to see if any contain a point.
        /// </summary>
        /// <param name="point">The point to be checked.</param>
        /// <returns>Whether any view in the list contained the point.</returns>
        public override bool Contains(Point point)
        {
            return _views.Any(v => v.Contains(point));
        }
    }
}