﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Class managing input and keeping track of previous keyboard and mouse states.
    /// </summary>
    public class InputHelper
    {
        private KeyboardState _currentKeyboardState;
        private MouseState _currentMouseState;
        private KeyboardState _previousKeyboardState;
        private MouseState _previousMouseState;

        /// <summary>
        /// Get the currount mouse position as a Vector2.
        /// </summary>
        public Vector2 MousePosition
        {
            get { return new Vector2(_currentMouseState.X, _currentMouseState.Y); }
        }

        /// <summary>
        /// Get the point position the mouse is it.
        /// </summary>
        public Point MousePoint
        {
            get { return new Point((int) MousePosition.X, (int) MousePosition.Y); }
        }

        /// <summary>
        /// Returns whether any key on the keyboard is pressed.
        /// </summary>
        public bool AnyKeyPressed
        {
            get
            {
                return _currentKeyboardState.GetPressedKeys().Length > 0 &&
                       _previousKeyboardState.GetPressedKeys().Length == 0;
            }
        }

        /// <summary>
        /// Update the current and previous states.
        /// </summary>
        public void Update()
        {
            _previousMouseState = _currentMouseState;
            _previousKeyboardState = _currentKeyboardState;
            _currentMouseState = Mouse.GetState();
            _currentKeyboardState = Keyboard.GetState();
        }

        /// <summary>
        /// Returns whether the left button of the mouse has been pressed.
        /// </summary>
        /// <returns></returns>
        public bool MouseLeftButtonPressed()
        {
            return _currentMouseState.LeftButton == ButtonState.Pressed &&
                   _previousMouseState.LeftButton == ButtonState.Released;
        }

        /// <summary>
        /// Returns whether the left button of the mouse is down.
        /// </summary>
        /// <returns></returns>
        public bool MouseLeftButtonDown()
        {
            return _currentMouseState.LeftButton == ButtonState.Pressed;
        }

        /// <summary>
        /// Returns whether the right button of the mouse has been pressed.
        /// </summary>
        /// <returns></returns>
        public bool MouseRightButtonPressed()
        {
            return _currentMouseState.RightButton == ButtonState.Pressed &&
                   _previousMouseState.LeftButton == ButtonState.Released;
        }

        /// <summary>
        /// Returns whether the right button of the mous is down.
        /// </summary>
        /// <returns></returns>
        public bool MouseRightButtonDown()
        {
            return _currentMouseState.RightButton == ButtonState.Pressed;
        }

        /// <summary>
        /// Returns whether the specified key has been pressed
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public bool KeyPressed(Keys k)
        {
            return _currentKeyboardState.IsKeyDown(k) && _previousKeyboardState.IsKeyUp(k);
        }

        /// <summary>
        /// Returns whether the specified key is down
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public bool IsKeyDown(Keys k)
        {
            return _currentKeyboardState.IsKeyDown(k);
        }
    }
}