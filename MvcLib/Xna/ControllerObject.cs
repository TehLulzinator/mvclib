﻿namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Handles input.
    /// </summary>
    public abstract class ControllerObject
    {
        /// <summary>
        /// The parent gameloop standing at the top of the hierarchy.
        /// </summary>
        public GameLoop GameLoop;

        /// <summary>
        /// Handles input on this object. Override to add custom input logic.
        /// </summary>
        /// <param name="inputHelper">The input helper used</param>
        public abstract void HandleInput(InputHelper inputHelper);
    }
}