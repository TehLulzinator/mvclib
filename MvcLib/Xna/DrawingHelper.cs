﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GameManagement
{
    public class DrawingHelper
    {
        protected static Texture2D pixel;
        protected static SpriteFont font;

        public static void Initialize(GraphicsDevice graphics, SpriteFont font)
        {
            pixel = new Texture2D(graphics, 1, 1);
            pixel.SetData(new[] { Color.White });
            DrawingHelper.font = font;
        }

        /// <summary>
        /// Draws a rectangle.
        /// </summary>
        public static void DrawRectangle(Rectangle r, SpriteBatch spriteBatch, Color col, int borderwidth = 1, bool filled = false)
        {
            if (!filled)
            {
                spriteBatch.Draw(pixel, new Rectangle(r.Left, r.Top, borderwidth, r.Height), col); // Left
                spriteBatch.Draw(pixel, new Rectangle(r.Left, r.Top, r.Width, borderwidth), col); // Top
                spriteBatch.Draw(pixel, new Rectangle(r.Left, r.Bottom, r.Width, borderwidth), col); // Bottom
                spriteBatch.Draw(pixel, new Rectangle(r.Right - borderwidth, r.Top, borderwidth, r.Height), col); // Right
            }
            else
            {
                spriteBatch.Draw(pixel, r, col);
            }
        }

        /// <summary>
        /// Draws a line from point to point.
        /// </summary>
        public static void DrawLine(SpriteBatch spriteBatch, Color color, Vector2 point1, Vector2 point2, float width = 1f)
        {
            float angle = (float)Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            float length = Vector2.Distance(point1, point2);

            spriteBatch.Draw(pixel, point1, null, color, angle, Vector2.Zero, new Vector2(length, width), SpriteEffects.None, 0);
        }

        /// <summary>
        /// Text alignment enum used in DrawString.
        /// </summary>
        public enum Alignment { Left, Right, Center, None }

        public static void DrawString(SpriteBatch spriteBatch, Color color, Vector2 pos, string text, Alignment alignment = Alignment.None, float scale = 1.0f, SpriteFont sFont = null)
        {
            Vector2 origin;
            Vector2 size;
            if (sFont == null)
            {
                sFont = font;
            }
            if (alignment == Alignment.Center)
            {
                size = sFont.MeasureString(text);
                origin = new Vector2(size.X / 2, size.Y / 2);
            }
            else if (alignment == Alignment.Right)
            {
                size = sFont.MeasureString(text);
                origin = new Vector2(size.X, size.Y / 2);
            }
            else if (alignment == Alignment.Left)
            {
                size = sFont.MeasureString(text);
                origin = new Vector2(0, size.Y / 2);
            }
            else
            {
                origin = new Vector2(0, 0);
            }
            spriteBatch.DrawString(sFont, text, pos, color, 0, origin, scale, SpriteEffects.None, 0);
        }

        public static void DrawCircle(SpriteBatch spriteBatch, Color color, Vector2 center, float radius)
        {
            int steps = (int)radius * 5;
            float x, y;
            for (int i = 0; i < steps; i++)
            {
                x = center.X + radius * (float)Math.Cos(i);
                y = center.Y + radius * (float)Math.Sin(i);
                spriteBatch.Draw(pixel, new Vector2(x, y), color);
            }
        }
    }
}