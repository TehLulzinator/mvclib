﻿using System;
using System.Collections.Generic;

namespace Herman.MvcLib.Xna
{
    /// <summary>
    /// Handles input on multiple <c>ControllerObject</c>s.
    /// </summary>
    public class ControllerList : ControllerObject
    {
        private List<ControllerObject> _controllers;

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="gameLoop"></param>
        public ControllerList()
        {
            _controllers = new List<ControllerObject>();
        }

        /// <summary>
        /// Add a controller to this list.
        /// </summary>
        /// <param name="controller">The controller to add.</param>
        public void Add(ControllerObject controller)
        {
            _controllers.Add(controller);
        }

        /// <summary>
        /// Removes a controller from the hierarchy.
        /// </summary>
        /// <param name="controller">The controller to be removed.</param>
        public void Remove(ControllerObject controller)
        {
            if (!_controllers.Remove(controller))
                throw new ArgumentException("Controller could not be removed from list or was not found.", "controller");
        }

        public override void HandleInput(InputHelper inputHelper)
        {
            foreach (ControllerObject c in _controllers)
                c.HandleInput(inputHelper);
        }
    }
}